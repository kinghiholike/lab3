 Syntax = proto3
 
 service users {
    rpc create_user (CreateRequest) returns (CreateResponse);
    rpc update_user (CreateRequest) returns (SingleUserResponse);
 }

 message CreateRequest{

       String username = 1;
       String firstname =2;
       String lastname = 3;
       String email = 4;
 }

 message CreateResponse{
    String userid = 1;

 }

 message SingleUserResponse{
    String firstname =1;
    String lastname =1;
    String email= 3;
 }